// create a function that accept single parameter. the parameter will be an array of integer
// your function should be able to count the unique value inside the array
// examples:
// countUniqueValues([1, 2, 3, 4, 4, 4, 7, 7, 12, 12, 13])) => 7
// countUniqueValues([1, 2, 2, 2, 2, 2, 2, 2, 4, 6])) => 4
// countUniqueValues([])) => 0

function a(arr) {
  let a = 0;
  for (let i = 0; i < arr.length; i++) {
    //methode untuk melakukan looping atau perulangan terhadap arr
    //let i itu menandakan pendeklarasian looping variabel i dengan dimulai dari index 0
    //1 < arr.length kondisi dimana dia memeriksa apakah i kurang dari angka yg berada dalam arr
    //  i++ adalah deklarasi menggunakan operator ++ untuk menambah nilai i=1,penambahan dilakukan di setiap akhir looping
    if (arr[i] != arr[i + 1]) {
      a += 1;
    }
    console.log("Ini arr i", arr[i], "Ini arr i+1", arr[i + 1]);
  }
  return a;
}

console.log(a([1, 2, 3, 4, 4, 4, 7, 7, 12, 12, 13]));
console.log(a([1, 2, 2, 2, 2, 2, 2, 2, 4, 6]));
console.log(a([]));
